namespace Hallowsim.util.states {
	using System;

	public abstract class AbstractState : IStatelikeObject
	{

		public abstract string name { get; }

		protected StateMachine _sm { get; private set; }

		public void initialize_state(StateMachine sm) {
			this._sm = sm;
		}

		public void transfer(string to) {
			this._sm.transfer(this.name, to);
		}

		public void push(string name) {
			this._sm.push(name);
		}

		public void pop() {
			if (this._sm.current != this.name) {
				throw new InvalidOperationException("Tried to pop a state that wasn't at the top of the stack.");
			}

			this._sm.pop();
		}

		public virtual void process() { }

		public virtual void physics_process(float delta) { }

		public virtual void input(Godot.InputEvent evt) { }

		public virtual void integrate_forces(Godot.Physics2DDirectBodyState state) { }

		public virtual void enter(string? from) { }

		public virtual void exit(string? to) { }

	}
}
