namespace Hallowsim.util.states {
	using System;
	using System.Collections.Immutable;
	using System.Diagnostics;
	using System.Collections.Generic;

	public class StateMachine : IStatelikeObject
	{

		private readonly Dictionary<string, AbstractState> _states = new Dictionary<string, AbstractState>();

		private readonly List<string> _stack = new List<string>();

		public StateMachine() { }

		public StateMachine(params AbstractState[] states) {
			foreach (var state in states) {
				this.add(state);
			}
		}

		public void process() {
			if (this._stack.Count > 0) {
				this._states[this._stack[this._stack.Count - 1]].process();
			}
		}

		public void physics_process(float delta) {
			if (this._stack.Count > 0) {
				this._states[this._stack[this._stack.Count - 1]].physics_process(delta);
			}
		}

		public void input(Godot.InputEvent evt) {
			if (this._stack.Count > 0) {
				this._states[this._stack[this._stack.Count - 1]].input(evt);
			}
		}

		public void integrate_forces(Godot.Physics2DDirectBodyState state) {
			if (this._stack.Count > 0) {
				this._states[this._stack[this._stack.Count - 1]].integrate_forces(state);
			}
		}

		public bool has(string name) {
			return this._states.ContainsKey(name);
		}

		public bool has(AbstractState state) {
			return this._states.ContainsValue(state);
		}

		public AbstractState get(string name) {
			return this._states[name];
		}

		public bool is_in_stack(string name) {
			return this._stack.Contains(name);
		}

		public bool is_in_stack(AbstractState state) {
			return this._stack.Contains(state.name);
		}

		public void add(AbstractState state) {
			this._states.Add(state.name, state);
			state.initialize_state(this);
		}

		public void start(string name) {

			if (this.current != null) {
				throw new InvalidOperationException("State machine has already started.");
			}

			if (this._stack.Count > 0) {
				throw new InvalidOperationException("State machine has already started and has a stack.");
			}

			this.push(name);

		}

		public string? previous {
			get {
				try {
					return this._stack[this._stack.Count - 2];
				}
				catch (ArgumentOutOfRangeException) {
					return null;
				}
			}
		}

		public string? current {
			get {
				try {
					return this._stack[this._stack.Count - 1];
				}
				catch (ArgumentOutOfRangeException) {
					return null;
				}
			}
		}

		public void push(string name) {
			Debug.Assert(this._states.ContainsKey(name), "Tried to push a new state to the stack that doesn't exist.");

			var old_current = this.current;
			this._stack.Add(name);
			var new_current = this.current;

			if (old_current != null) {
				this._states[old_current].exit(new_current);
			}

			if (new_current != null) {
				this._states[new_current].enter(old_current);
			}

		}

		public void pop() {
			var old_current = this.current;
			this._try_pop_end();
			var new_current = this.current;

			if (old_current != null) {
				this._states[old_current].exit(new_current);
			}

			if (new_current != null) {
				this._states[new_current].enter(old_current);
			}

		}

		public void transfer(string current, string? to)
		{
			// We can assume that this method will only be called if a state already is in the stack.
			this._stack.RemoveAt(this._stack.Count - 1);
			this._states[current].exit(to);

			if (to != null) {
				Debug.Assert(this._states.ContainsKey(to));
				this._stack.Add(to);

				this._states[to].enter(current);
			}

		}

		private void _try_pop_end() {
			try {
				this._stack.RemoveAt(this._stack.Count - 1);
			}
			catch (ArgumentOutOfRangeException) {
				// Do nothing, this occurs if the length is 0. We don't care if it happens.
			}
		}
	}
}
