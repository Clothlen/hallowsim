namespace Hallowsim.util.states {
	public interface IStatelikeObject
	{

		void process();

		void physics_process(float delta);

		void input(Godot.InputEvent evt);

		void integrate_forces(Godot.Physics2DDirectBodyState state);

	}
}
