namespace Hallowsim.util.objects {
	public interface IIntegrateForces
	{
		void integrate_forces(Godot.Physics2DDirectBodyState state);
	}
}
