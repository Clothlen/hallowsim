namespace Hallowsim.util.objects {
	using System.Diagnostics;
	using System;

	public abstract class AbstractAspect : IObjectComponent, IDisposable
	{

		public AspectContainer container;

		private bool _disposed = false;

		public virtual void initialize_aspect(AspectContainer container) {
			this.container = container;
			this.init();
		}

		public virtual void init() { }

		public virtual void start() { }

		public virtual void stop() { }

		public virtual void process() { }

		public virtual void physics_process(float delta) { }

		public virtual void input(Godot.InputEvent evt) { }


		public void subscribe() {
			// No, you cannot turn this into a switch statement.
			if (this is IProcess process_interface) {
				this.container.process_event += process_interface.process;
			}

			if (this is IPhysicsProcess physics_process_interface) {
				this.container.physics_process_event += physics_process_interface.physics_process;
			}

			if (this is IInput input_interface) {
				this.container.input_event += input_interface.input;
			}

			if (this is IIntegrateForces integrate_forces_interface) {
				this.container.integrate_forces_event += integrate_forces_interface.integrate_forces;
			}
		}

		public void unsubscribe() {
			if (this is IProcess process_interface) {
				this.container.process_event -= process_interface.process;
			}

			if (this is IPhysicsProcess physics_process_interface) {
				this.container.physics_process_event -= physics_process_interface.physics_process;
			}

			if (this is IInput input_interface) {
				this.container.input_event -= input_interface.input;
			}

			if (this is IIntegrateForces integrate_forces_interface) {
				this.container.integrate_forces_event -= integrate_forces_interface.integrate_forces;
			}
		}

		public void Dispose() {
			this._dispose(true);
			GC.SuppressFinalize(this);
		}

		// Destroy managed resources that are under .NET's control.
		private void _dispose_managed() {
			// TODO: bad way of doing this?
			this.unsubscribe();
		}

		// Destroy items that are beyond .NET's control.
		// Like direct file handles, direct network connections, things written in
		// C++, etc.
		private void _dispose_unmanaged() { }

		private void _dispose(bool release_managed) {
			if (this._disposed) {
				return;
			}
			this._disposed = true;

			if (release_managed) {
				this._dispose_managed();
			}

			this._dispose_unmanaged();
		}

		~AbstractAspect() {
			this._dispose(false);
		}
	}
}
