namespace Hallowsim.util.objects {
	public interface IPhysicsProcess
	{
		void physics_process(float delta);
	}
}
