namespace Hallowsim.util.objects {
	using System.Diagnostics;
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public class ServiceContainer : IObjectComponentContainer<AbstractService>, IDisposable
	{

		public event ProcessHandler process_event;

		public event PhysicsProcessHandler physics_process_event;

		public event InputHandler input_event;

		public event IntegrateForcesHandler integrate_forces_event;

		private readonly HashSet<AbstractService> _services = new HashSet<AbstractService>();

		private bool _disposed = false;

		/**
		* <summary>
		* Check if a service has already been added.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		* <returns>If it exists or not.</returns>
		*/
		public bool has<T>()
		where T : AbstractService {
			foreach (var thing in this._services) {
				if (thing is T) {
					return true;
				}
			}

			return false;
		}

		public bool has(Type type) {
			foreach (var thing in this._services) {
				if (type.IsInstanceOfType(thing)) {
					return true;
				}
			}

			return false;
		}

		/**
		* <summary>
		* Get the first service instance of a given type.
		*
		* If none could be found, a System.ArgumentException will be thrown.
		* </summary>
		* <typeparam name="T">The type to look for.</typeparam>
		* <returns>The first found instance.</returns>
		*/
		public T get<T>()
		where T : AbstractService {
			foreach (var serv in this._services) {
				if (serv is T returner) {
					return returner;
				}
			}

			throw new ArgumentException($"The service {typeof(T).FullName} was not found.");
		}

		public AbstractService get(Type type) {
			foreach (var serv in this._services) {
				if (type.IsInstanceOfType(serv)) {
					return serv;
				}
			}

			throw new ArgumentException($"The service {type.FullName} was not found.");
		}

		/**
		* <summary>
		* Try to get an service instance of a given type.
		* </summary>
		* <typeparam name="T">The type to look for.</typeparam>
		* <returns>An instance of that type, or null if none could be found.</returns>
		*/
		public T? try_get<T>()
		where T : AbstractService {
			foreach (var service in this._services) {
				if (service is T returner) {
					return returner;
				}
			}

			return null;
		}

		// // You might be wondering; why can't you write it like this?:
		// // ```cs
		// // 	public T? try_get<T>()
		// // 	where T : AbstractService
		// // ```
		// // The reason for this is due to this error:
		// // `The constraints for type parameter 'T' of method 'ServiceContainer.try_get<T>()' must match the constraints for type parameter 'T' of interface method 'IObjectComponentContainer<AbstractService>.try_get<T>()'. Consider using an explicit interface implementation instead.`
		// // This is the best way, seemingly.
		// // Also, see https://stackoverflow.com/questions/1868861/how-to-call-explicit-interface-implementation-methods-internally-without-explici
		// T? IObjectComponentContainer<AbstractService>.try_get<T>()
		// where T : class {
		// 	foreach (var service in this._services) {
		// 		if (service is T returner) {
		// 			return returner;
		// 		}
		// 	}

		// 	return null;
		// }

		public AbstractService? try_get(Type type) {
			foreach (var service in this._services) {
				if (type.IsInstanceOfType(service)) {
					return service;
				}
			}

			return null;
		}

		/**
		* <summary>
		* Get all services.
		* </summary>
		* <returns>IEnumerable</returns>
		*/
		public IEnumerable<AbstractService> get_enumerable() {
			foreach (var service in this._services) {
				yield return service;
			}

			yield break;
		}

		/**
		* <summary>
		* Get all services of a given type.
		* </summary>
		* <typeparam name="T">The type to filter for.</typeparam>
		* <returns>Enumerable of the given type.</returns>
		*/
		public IEnumerable<T> get_enumerable<T>()
		where T : AbstractService {
			foreach (var service in this._services) {
				if (service is T yielder) {
					yield return yielder;
				}
			}

			yield break;
		}

		public IEnumerable<AbstractService> get_enumerable(Type type) {
			foreach (var service in this._services) {
				if (type.IsInstanceOfType(service)) {
					yield return service;
				}
			}

			yield break;
		}

		/**
		* <summary>
		* Get all services as an array.
		* </summary>
		* <returns>Array of services.</returns>
		*/
		public AbstractService[] get_all() {
			return this.get_enumerable().ToArray();
		}

		/**
		* <summary>
		* Get all services of a given type as an array.
		* </summary>
		* <typeparam name="T">The type to filter for.</typeparam>
		* <returns>An array of service instances of the type.</returns>
		*/
		public T[] get_all<T>()
		where T : AbstractService {
			return this.get_enumerable<T>().ToArray();
		}

		public AbstractService[] get_all(Type type) {
			return this.get_enumerable(type).ToArray();
		}

		/**
		* <summary>
		* Get all services as a list.
		* </summary>
		* <returns>A list of services.</returns>
		*/
		public List<AbstractService> get_all_as_list() {
			return new List<AbstractService>(this.get_enumerable());
		}

		/**
		* <summary>
		* Get all services of a given type as a list.
		* </summary>
		* <typeparam name="T">The type to filter for.</typeparam>
		* <returns>A list of services of a given type.</returns>
		*/
		public List<T> get_all_as_list<T>()
		where T : AbstractService {
			return new List<T>(this.get_enumerable<T>());
		}

		public List<AbstractService> get_all_as_list(Type type) {
			return new List<AbstractService>(this.get_enumerable(type));
		}

		/**
		* <summary>
		* Add a new service instance of a given type.
		* </summary>
		* <typeparam name="T">The type to create.</typeparam>
		* <returns>The newly created instance.</returns>
		*/
		public T add<T>()
		where T : AbstractService, new() {
			var inst = new T();
			this._services.Add(inst);
			return inst;
		}

		public AbstractService add(Type type) {
			var inst = (AbstractService)Activator.CreateInstance(type);
			this._services.Add(inst);
			return inst;
		}

		/**
		* <summary>
		* Add a new service.
		* </summary>
		* <param name="instance">The instance to add.</param>
		* <returns>The provided instance.</returns>
		*/
		public AbstractService add(AbstractService instance) {
			this._services.Add(instance);
			return instance;
		}

		/**
		* <summary>
		* Get a service, or add it if it does not exist.
		* </summary>
		* <typeparam name="T">The type of the service.</typeparam>
		* <returns>The service instance.</returns>
		*/
		public T get_or_add<T>()
		where T : AbstractService, new() {
			var existing = this.try_get<T>();
			if (existing != null) {
				return existing;
			}

			return this.add<T>();
		}

		public AbstractService get_or_add(Type type) {
			var existing = this.try_get(type);
			if (existing != null) {
				return existing;
			}

			return this.add(type);
		}

		/**
		* <summary>
		* Remove the first found service instance of a given type.
		*
		* Be careful using this as it may not get exactly what you are looking for.
		* </summary>
		* <typeparam name="T">The type to look for.</typeparam>
		*/
		public void remove<T>()
		where T : AbstractService {
			T? remover = null;
			foreach (var thing in this._services) {
				if (thing is T remover2) {
					remover = remover2;
					break;
				}
			}

			if (remover == null) {
				throw new ArgumentException($"The service {typeof(T).FullName} could not be removed because it was not found.");
			}

			remover.stop();
			remover.Dispose();
			this._services.Remove(remover);
		}

		public void remove(Type type) {
			AbstractService? remover = null;
			foreach (var thing in this._services) {
				if (type.IsInstanceOfType(thing)) {
					remover = thing;
				}
			}

			if (remover == null) {
				throw new ArgumentException($"The service {type.FullName} could not be removed because it was not found.");
			}

			remover.stop();
			remover.Dispose();
			this._services.Remove(remover);
		}

		/**
		* <summary>
		* Remove a particular service instance.
		* </summary>
		* <param name="service">The instance to remove.</param>
		*/
		public void remove(AbstractService service) {
			service.stop();
			service.Dispose();
			this._services.Remove(service);
		}

		/**
		* <summary>
		* Remove all service instances of a given type.
		* </summary>
		* <typeparam name="T">The type to remove.</typeparam>
		*/
		public void remove_all_of<T>()
		where T : AbstractService {
			// TODO: we could possibly reduce this to one foreach loop.
			// Create a new hashset, add items we want to keep to it, and then
			// overwrite the old hashset.
			var to_be_removed = new List<T>();
			foreach (var serv in this._services) {
				if (serv is T remover) {
					to_be_removed.Add(remover);
				}
			}

			foreach (var remover in to_be_removed) {
				this._services.Remove(remover);
				remover.stop();
				remover.Dispose();
			}
		}

		public void remove_all_of(Type type) {
			var to_be_removed = new List<AbstractService>();
			foreach (var serv in this._services) {
				if (type.IsInstanceOfType(serv)) {
					to_be_removed.Add(serv);
				}
			}

			foreach (var remover in to_be_removed) {
				this._services.Remove(remover);
				remover.stop();
				remover.Dispose();
			}
		}

		/**
		* <summary>
		* Remove all services.
		* </summary>
		*/
		public void clear() {
			foreach (var service in this._services) {
				service.stop();
				service.Dispose();
			}
			this._services.Clear();
		}

		/**
		* <summary>
		* Assert that a service of a given type exists.
		*
		* Only useful in Debug mode.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		*/
		public void assert_exists<T>()
		where T : AbstractService {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (serv is T) {
						found = true;
						break;
					}
				}

				Debug.Assert(found, $"ServiceContainer does not contain service {typeof(T).FullName}.");
			#endif
		}

		public void assert_exists(Type type) {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (type.IsInstanceOfType(serv)) {
						found = true;
						break;
					}
				}

				Debug.Assert(found, $"ServiceContainer does not contain service {type.FullName}.");
			#endif
		}

		/**
		* <summary>
		* Assert that a service exists.
		*
		* Only useful in Debug mode.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		*/
		public void assert_not_exists<T>()
		where T : AbstractService {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (serv is T) {
						found = true;
						break;
					}
				}

				Debug.Assert(!found, $"ServiceContainer contains service {typeof(T).FullName}.");
			#endif
		}

		public void assert_not_exists(Type type) {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (type.IsInstanceOfType(serv)) {
						found = true;
						break;
					}
				}

				Debug.Assert(!found, $"ServiceContainer contains service {type.FullName}.");
			#endif
		}

		/**
		* <summary>
		* Require that a particular type of service exists.
		*
		* If it does, we will get it. If not, a System.ArgumentException will occur.
		*
		* If multiple instances exist, it will return the first found instance.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		* <returns>The service instance.</returns>
		*/
		public T require<T>()
		where T : AbstractService {
			return this.get<T>();
		}

		public AbstractService require(Type type) {
			return this.get(type);
		}

		/**
		* <summary>
		* Ask that a particular type of service exists.
		*
		* If it does, it will be returned. If not, null is returned.
		*
		* If multiple instances exist, it will return the first found instance.
		* </summary>
		* <typeparam name="T"></typeparam>
		* <returns></returns>
		*/
		public T? import<T>()
		where T : AbstractService {
			return this.try_get<T>();
		}

		public AbstractService? import(Type type) {
			return this.try_get(type);
		}

		/**
		* <summary>
		* Start all service instances.
		* </summary>
		*/
		public void start() {
			foreach (var serv in this._services) {
				serv.start();
			}
		}

		/**
		* <summary>
		* Stop and destroy all service instances.
		* </summary>
		*/
		public void stop() {
			this.clear();
		}

		// These next four methods are for Godot usage.
		public void process() {
			this.process_event?.Invoke();
		}

		public void physics_process(float delta) {
			this.physics_process_event?.Invoke(delta);
		}

		public void input(Godot.InputEvent evt) {
			this.input_event?.Invoke(evt);
		}

		public void integrate_forces(Godot.Physics2DDirectBodyState state) {
			this.integrate_forces_event?.Invoke(state);
		}

		public void Dispose() {
			this._dispose();
			GC.SuppressFinalize(this);
		}

		private void _dispose() {
			if (this._disposed) {
				return;
			}
			this._disposed = true;

			this.clear();
		}

	}
}
