namespace Hallowsim.util.objects {
	public interface IObjectComponent
	{

		/**
		* <summary>
		* Initialize the component.
		*
		* You should ask for other components here.
		* </summary>
		*/
		void init();

		/**
		* <summary>
		* Start the component.
		* </summary>
		*/
		void start();

		/**
		* <summary>
		* Stop the component.
		* </summary>
		*/
		void stop();

	}
}
