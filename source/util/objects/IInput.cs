namespace Hallowsim.util.objects {
	public interface IInput
	{
		void input(Godot.InputEvent evt);
	}
}
