namespace Hallowsim.util.objects {
	using System;

	public interface IObjectComponentContainer<TDerived> : IProcess, IPhysicsProcess, IInput, IIntegrateForces
	where TDerived : class, IObjectComponent
	{

		event ProcessHandler process_event;

		event PhysicsProcessHandler physics_process_event;

		event InputHandler input_event;

		event IntegrateForcesHandler integrate_forces_event;

		bool has<T>()
		where T : TDerived;

		bool has(Type type);

		T get<T>()
		where T : TDerived;

		TDerived get(Type type);

		// Note: T is nullable!
		// However, we cannot display it here due to C#.
		// Attempting to set it to `T?` will ask you to constrain T to
		// a nullable reference type. You could try `where T : class, TDerived`.
		// However, implementing classes cannot implement it correctly then.
		// For example, AspectContainer cannot implement it due to
		// not being to specify `where T : class, AbstractAspect`.
		T try_get<T>()
		where T : TDerived;

		TDerived? try_get(Type type);

		T add<T>()
		where T : TDerived, new();

		TDerived add(Type type);

		T get_or_add<T>()
		where T : TDerived, new();

		TDerived get_or_add(Type type);

		void remove<T>()
		where T : TDerived;

		void remove(Type type);

		void clear();

		void assert_exists<T>()
		where T : TDerived;

		void assert_not_exists<T>()
		where T : TDerived;

		void start();

		void stop();

	}

	public delegate void ProcessHandler();

	public delegate void PhysicsProcessHandler(float delta);

	public delegate void InputHandler(Godot.InputEvent evt);

	public delegate void IntegrateForcesHandler(Godot.Physics2DDirectBodyState state);
}
