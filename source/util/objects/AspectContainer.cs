namespace Hallowsim.util.objects {
	using System.Diagnostics;
	using System.Linq;
	using System;
	using System.Collections.Generic;

	public class AspectContainer : IObjectComponentContainer<AbstractAspect>
	{
		public event ProcessHandler process_event;

		public event PhysicsProcessHandler physics_process_event;

		public event InputHandler input_event;

		public event IntegrateForcesHandler integrate_forces_event;

		private readonly Dictionary<Type, AbstractAspect> _aspects = new Dictionary<Type, AbstractAspect>();

		// TODO: possibly set this to null once we `start()`?
		// We don't ever be using it afterwards, unless we `stop()`.
		private readonly Dictionary<Type, AbstractAspect> _unmet_aspects = new Dictionary<Type, AbstractAspect>();

		private bool _running = false;

		/**
		* <summary>
		* Check if an aspect has already been added or not.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		* <returns>If it exists or not.</returns>
		*/
		public bool has<T>()
		where T : AbstractAspect {
			return this._aspects.ContainsKey(typeof(T));
		}

		public bool has(Type type) {
			return this._aspects.ContainsKey(type);
		}

		/**
		* <summary>
		* Get an aspect.
		*
		* Throws System.KeyNotFoundException if the aspect hasn't been added.
		* </summary>
		* <typeparam name="T">The type to get.</typeparam>
		* <returns>The instance of that type.</returns>
		*/
		public T get<T>()
		where T : AbstractAspect {
			return (T)this._aspects[typeof(T)];
		}

		public AbstractAspect get(Type type) {
			return this._aspects[type];
		}

		/**
		* <summary>
		* Try to get an aspect.
		*
		* If the aspect doesn't exist, null is returned.
		*
		* Operates under the "it's better to ask for forgiveness than for
		* permission" principle. If you highly suspect this will return null, it
		* may be faster to use `try_get_perm` instead.
		* </summary>
		* <typeparam name="T">The type to get.</typeparam>
		* <returns>The instance of that type, or null.</returns>
		*/
		public T? try_get<T>()
		where T : AbstractAspect {
			try {
				return (T)this._aspects[typeof(T)];
			}
			catch (KeyNotFoundException) {
				return null;
			}
		}

		public AbstractAspect? try_get(Type type) {
			try {
				return this._aspects[type];
			}
			catch (KeyNotFoundException) {
				return null;
			}
		}

		/**
		* <summary>
		* Try to get an aspect.
		*
		* If the aspect doesn't exist, null is returned.
		*
		* Operates under the "it's better to ask for permission than for
		* forgiveness" principle. If you highly suspect this will not return null,
		* it may be faster to use `try_get` instead.
		* </summary>
		* <typeparam name="T">The type to get.</typeparam>
		* <returns>The instance of that type, or null.</returns>
		*/
		public T? try_get_perm<T>()
		where T : AbstractAspect {
			if (this._aspects.ContainsKey(typeof(T))) {
				return (T)this._aspects[typeof(T)];
			}

			return null;
		}

		public AbstractAspect? try_get_perm(Type type) {
			if (this._aspects.ContainsKey(type)) {
				return this._aspects[type];
			}

			return null;
		}

		/**
		* <summary>
		* Get all of the types in this container.
		* </summary>
		* <returns>IEnumerable of all types.</returns>
		*/
		public IEnumerable<Type> get_enumerable_types() {
			foreach (var type in this._aspects.Keys) {
				yield return type;
			}

			yield break;
		}

		/**
		* <summary>
		* Get all aspect instances.
		* </summary>
		* <returns>IEnumerable of all aspects.</returns>
		*/
		public IEnumerable<AbstractAspect> get_enumerable() {
			foreach (var aspect in this._aspects.Values) {
				yield return aspect;
			}

			yield break;
		}

		/**
		* <summary>
		* Get all aspect instances as an array.
		* </summary>
		* <returns>An array of aspect instances.</returns>
		*/
		public AbstractAspect[] get_all() {
			return this.get_enumerable().ToArray();
		}

		/**
		* <summary>
		* Get all aspect instances as a list.
		* </summary>
		* <returns>A list of aspect instances.</returns>
		*/
		public List<AbstractAspect> get_all_as_list() {
			return new List<AbstractAspect>(this.get_enumerable());
		}

		/**
		* <summary>
		* Add a new aspect.
		*
		* If the aspect already exists, System.ArgumentException is thrown.
		* </summary>
		* <typeparam name="T">The type to add.</typeparam>
		* <returns>The instance of that type.</returns>
		*/
		public T add<T>()
		where T : AbstractAspect, new() {
			var type = typeof(T);
			T instance;
			if (this._unmet_aspects.ContainsKey(type)) {
				instance = (T)this._unmet_aspects[type];
				this._unmet_aspects.Remove(type);
			}
			else {
				instance = new T();
			}

			this._aspects.Add(type, instance);
			instance.initialize_aspect(this);

			return instance;
		}

		public AbstractAspect add(Type type) {
			AbstractAspect instance;
			if (this._unmet_aspects.ContainsKey(type)) {
				instance = this._unmet_aspects[type];
				this._unmet_aspects.Remove(type);
			}
			else {
				instance = (AbstractAspect)Activator.CreateInstance(type);
			}

			this._aspects.Add(type, instance);
			instance.initialize_aspect(this);

			return instance;
		}

		/**
		* <summary>
		* Get an aspect. If the aspect doesn't exist, it will be added first.
		* </summary>
		* <typeparam name="T">The type to get.</typeparam>
		* <returns>The instance of that type.</returns>
		*/
		public T get_or_add<T>()
		where T : AbstractAspect, new() {
			var existing = this.try_get<T>();
			if (existing != null) {
				return existing;
			}

			return this.add<T>();
		}

		public AbstractAspect get_or_add(Type type) {
			var existing = this.try_get(type);
			if (existing != null) {
				return existing;
			}

			return this.add(type);
		}

		/**
		* <summary>
		* Remove an aspect type.
		*
		* If the aspect does not exist, throws System.KeyNotFoundException.
		* </summary>
		* <typeparam name="T">The type to remove.</typeparam>
		*/
		public void remove<T>()
		where T : AbstractAspect {
			var aspect = this._aspects[typeof(T)];
			aspect.stop();
			aspect.Dispose();
			this._aspects.Remove(typeof(T));
		}

		public void remove(Type type) {
			var aspect = this._aspects[type];
			aspect.stop();
			aspect.Dispose();
			this._aspects.Remove(type);
		}

		/**
		* <summary>
		* Remove an aspect type, and return if it succeeded or not.
		*
		* If the aspect does not exist, throws System.KeyNotFoundException.
		*
		* This is unique to AspectContainer because the interface requires `void`
		* normally.
		* </summary>
		* <typeparam name="T">The type to remove.</typeparam>
		* <returns>If it succeeded or not.</returns>
		*/
		public bool remove_bool<T>()
		where T : AbstractAspect {
			var aspect = this._aspects[typeof(T)];
			aspect.stop();
			aspect.Dispose();
			return this._aspects.Remove(typeof(T));
		}

		public bool remove_bool(Type type) {
			var aspect = this._aspects[type];
			aspect.stop();
			aspect.Dispose();
			return this._aspects.Remove(type);
		}

		/**
		* <summary>
		* Remove all aspects.
		* </summary>
		*/
		public void clear() {
			foreach (var aspect in this._aspects.Values) {
				aspect.stop();
				aspect.Dispose();
			}

			this._aspects.Clear();
			this._unmet_aspects.Clear();
		}

		// Can't use this here:
		// [Conditional("DEBUG")]
		/**
		* <summary>
		* Assert that an aspect exists.
		*
		* Only useful in Debug mode.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		*/
		public void assert_exists<T>()
		where T : AbstractAspect {
			Debug.Assert(this.has<T>(), $"AspectContainer does not contain aspect {typeof(T).FullName}");
		}

		public void assert_exists(Type type) {
			Debug.Assert(this.has(type), $"AspectContainer does not contain aspect {type.FullName}");
		}

		/**
		* <summary>
		* Assert that an aspect does not exist.
		*
		* Only useful in Debug mode.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		*/
		public void assert_not_exists<T>()
		where T : AbstractAspect {
			Debug.Assert(!this.has<T>(), $"AspectContainer contains aspect {typeof(T).FullName}");
		}

		public void assert_not_exists(Type type) {
			Debug.Assert(!this.has(type), $"AspectContainer contains aspect {type.FullName}");
		}

		/**
		* <summary>
		* Demand that an aspect already exist.
		*
		* If it does, it will be returned to you.
		*
		* If it does not, an UnmetAspectException will be thrown.
		* </summary>
		* <typeparam name="T">The type to demand.</typeparam>
		* <returns>An instance of that type.</returns>
		*/
		public T demand<T>()
		where T : AbstractAspect, new() {
			if (this._running) {
				throw new UnmetAspectException();
			}

			var instance = new T();
			this._unmet_aspects.Add(typeof(T), instance);
			return instance;
		}

		/**
		* <summary>
		* Start all of the aspects.
		* </summary>
		*/
		public void start() {
			// TODO: Debug.Assert instead?
			if (this._running) {
				throw new AlreadyRunningException();
			}

			this._running = true;
			if (this._unmet_aspects!.Count > 0) {
				throw new UnmetAspectException();
			}

			foreach (var aspect in this._aspects.Values) {
				aspect.start();
			}
		}

		/**
		* <summary>
		* Stop all of the aspects.
		* </summary>
		*/
		public void stop() {
			this._running = false;

			foreach (var aspect in this._aspects.Values) {
				aspect.stop();
			}
		}

		// These next four methods are for Godot usage.
		public void process() {
			this.process_event?.Invoke();
		}

		public void physics_process(float delta) {
			this.physics_process_event?.Invoke(delta);
		}

		public void input(Godot.InputEvent evt) {
			this.input_event?.Invoke(evt);
		}

		public void integrate_forces(Godot.Physics2DDirectBodyState state) {
			this.integrate_forces_event?.Invoke(state);
		}

		/**
		* <summary>
		* An aspect was demanded, but it was not met.
		* </summary>
		*/
		[Serializable]
		public class UnmetAspectException : Exception
		{
			public UnmetAspectException() : base("An aspect was not met.") { }
		}

		/**
		* <summary>
		* The AspectContainer was asked to start running even though it already
		* was running.
		* </summary>
		*/
		[Serializable]
		public class AlreadyRunningException : Exception
		{
			public AlreadyRunningException() : base("AspectContainer already running.") { }
		}

	}
}
