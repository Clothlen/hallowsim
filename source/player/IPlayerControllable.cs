namespace Hallowsim.player {
	public interface IPlayerControllable
	{
		// // These controls are intended to be used for a Knight or Knight-like
		// // character, but it is left vague enough that it can be used for any entity.

		// void player_jump();

		void player_melee_attack();

		// // TODO: nail arts and other actions where you hold down a button.

		// void player_heal();

		// void player_secondary_attack();

		// // For like dream nails and such.
		// void player_special();

		// void player_dash();

		// void player_super_dash();

		// void player_up();

		// void player_down();

		// void player_left();

		// void player_right();

	}
}
