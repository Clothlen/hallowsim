namespace Hallowsim.scenes {
	using Godot;
	using System;

	public class AnimatedSprite : Node2D
	{

		[Export]
		public string[] resources;

		[Export]
		public string? initial;

		[Export]
		public bool flip_horizontal = false;

		[Export]
		public bool flip_vertical = false;

		// This is exposed so that parent scripts can find our node.
		public objects.images.AnimationSprite sprite;

		public override void _Ready() {
			if (this.resources == null || this.resources.Length < 1) {
				throw new ArgumentException($"Resources not set on animated sprite at node path {this.GetPath()}.");
			}

			string initial = this.initial!;
			if (initial == null) {
				initial = this.resources[0];
			}

			this.sprite = new objects.images.AnimationSprite(this.resources);
			this.sprite.init();
			this.sprite.flip_horizontal = this.flip_horizontal;
			this.sprite.flip_vertical = this.flip_vertical;
			this.sprite.play(initial);

			// anisprite.node.Transform = anisprite.node.GetRelativeTransformToParent(this);
			this.sprite.node.Position += this.Position;
			this.GetParent().CallDeferred("add_child_below_node", this, this.sprite.node, true);
			this.GetParent().CallDeferred("remove_child", this);

		}

	}
}
