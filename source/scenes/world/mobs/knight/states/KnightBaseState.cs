namespace Hallowsim.scenes.world.mobs.knight.states {
	public abstract class KnightBaseState : util.states.AbstractState
	{

		protected readonly Knight _knight;

		public KnightBaseState(Knight knight) {
			this._knight = knight;
		}
	}
}
