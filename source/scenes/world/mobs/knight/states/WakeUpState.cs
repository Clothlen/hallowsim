namespace Hallowsim.scenes.world.mobs.knight.states {
	using Godot;

	public class WakeUpState : KnightBaseState
	{

		public static readonly float TIME = 0.43f;

		public override string name { get { return "wake_up"; } }

		public WakeUpState(Knight knight) : base(knight) { }

		public override async void enter(string? from) {
			this._knight.sprite.play("vanilla:clips/Knight/Wake Up Ground");

			GD.Print("Entered state.");
			// TODO: could use Task.Delay
			await this._knight.ToSignal(this._knight.GetTree().CreateTimer(TIME), "timeout");
			this.pop();

		}

		public override void exit(string? to) {
		}

	}
}
