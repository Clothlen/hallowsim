namespace Hallowsim.scenes.world.mobs.knight.states {
	using Godot;

	public class AttackState : KnightBaseState
	{

		public static readonly float TIME = 0.43f;

		public override string name { get { return "attack"; } }

		public AttackState(Knight knight) : base(knight) { }

		public override async void enter(string? from) {
			this._knight.condition.allow_input = false;
			if (GD.Randf() > 0.5) {
				this._knight.sprite.play("vanilla:clips/Knight/Slash");
			}
			else {
				this._knight.sprite.play("vanilla:clips/Knight/SlashAlt");
			}

			GD.Print("Entered state.");
			// TODO: could use Task.Delay
			await this._knight.ToSignal(this._knight.GetTree().CreateTimer(TIME), "timeout");
			this.pop();

		}

		public override void exit(string? to) {
			this._knight.condition.allow_input = true;
		}

	}
}
