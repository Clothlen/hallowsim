namespace Hallowsim.scenes.world.mobs.knight.states {
	public class IdleState : KnightBaseState
	{

		public override string name { get { return "idle"; } }

		public IdleState(Knight knight) : base(knight) { }

		public override void enter(string? from) {
			this._knight.sprite.play("vanilla:clips/Knight/Idle");
		}

	}
}
