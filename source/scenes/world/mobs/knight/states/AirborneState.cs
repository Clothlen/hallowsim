namespace Hallowsim.scenes.world.mobs.knight.states {
	public class AirborneState : KnightBaseState
	{

		public override string name { get { return "airborne"; } }

		public AirborneState(Knight knight) : base(knight) { }

		public override void enter(string? from) {
			this._knight.sprite.play("vanilla:Knight/Airborne");
		}
	}
}
