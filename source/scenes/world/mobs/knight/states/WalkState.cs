namespace Hallowsim.scenes.world.mobs.knight.states {
	public class WalkState : KnightBaseState
	{

		public override string name { get { return "walk"; } }

		public WalkState(Knight knight) : base(knight) { }

		public override void enter(string? from) {
			this._knight.sprite.play("vanilla:clips/Knight/Walk");
		}

	}
}
