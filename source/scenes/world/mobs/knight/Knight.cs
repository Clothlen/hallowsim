namespace Hallowsim.scenes.world.mobs.knight {
	using Godot;

	public class Knight : AbstractMob, Hallowsim.player.IPlayerControllable
	{

		public objects.images.AnimationSprite sprite;

		public KnightCondition condition = new KnightCondition();

		private util.states.StateMachine _sm;

		public override void init() {
			this.sprite = this.GetNode<scenes.AnimatedSprite>("AnimatedSprite").sprite;

			this._sm = new util.states.StateMachine(
				new states.IdleState(this),
				new states.WalkState(this),
				new states.AirborneState(this),
				new states.AttackState(this),
				new states.WakeUpState(this)
			);
			this._sm.start("wake_up");

		}

		public override void _PhysicsProcess(float delta) {
			this._sm.physics_process(delta);
		}

		public override void _IntegrateForces(Physics2DDirectBodyState state) {
			this._sm.integrate_forces(state);
		}

		public void player_melee_attack() {
			if (!this.condition.allow_input) {
				return;
			}

			if (!this.condition.can_attack) {
				return;
			}

			this._sm.push("attack");
		}

	}
}
