namespace Hallowsim.scenes.world.mobs {
	public abstract class AbstractMob : Godot.RigidBody2D
	{

		public abstract void init();

		public override void _Ready() {
			this.init();
		}
	}
}
