namespace Hallowsim.scenes.world {
	using Godot;

	public class World : Node2D
	{

		public Node2D? camera_target = null;

		private environments.AbstractEnvironment _environment;

		private Camera2D _camera;

		private player.Player _player;

		public override void _Ready() {
			this._camera = this.GetNode<Camera2D>("Camera2D");

			var environment_scene = ResourceLoader.Load<PackedScene>("res://source/scenes/world/environments/TestEnvironment.tscn");

			this._environment = (environments.AbstractEnvironment)environment_scene.Instance();
			this.AddChild(this._environment);

			this._player = new player.Player();
			// this._environment.AddChild(this._player.entity.node);
			// this.camera_target = this._player.entity.node;

			this._environment.AddChild(this._player.node);
			this.camera_target = this._player.node;

		}

		public override void _PhysicsProcess(float delta) {
			if (this.camera_target != null) {
				this._camera.Position = this.camera_target.Position;
			}
		}

		public override void _Input(InputEvent evt) {
			this._player.input(evt);
		}

	}
}
