namespace Hallowsim.scenes.world.environments {
	using Godot;

	public class TestEnvironment : AbstractEnvironment
	{

		public override void _Ready() {
			// Make some walls and floors automagically.
			var bridge = this.GetNode<Node2D>("Bridge");

			for (int i = 0; i < 30; i++) {
				const int width = 391 - 2;
				var sprite = new objects.images.AnimationSprite("vanilla:sprites/ruind_bridge_roof_01");
				sprite.init();
				sprite.play("vanilla:sprites/ruind_bridge_roof_01");
				sprite.node.Name = "bgwall";

				sprite.node.Position = new Vector2(width * i, 0);
				bridge.AddChild(sprite.node);
			}

			int floor_head = 0;
			GD.Randomize();
			for (int i = 0; i < 30; i++) {
				int width = 586 - 2;
				var path = "vanilla:collections/Church/church_pieces_0005_floor";

				if (GD.Randf() > 0.5) {
					path = "vanilla:collections/Church/church_pieces_0006_floor";
					width = 508 - 2;
				}

				var sprite = new objects.images.AnimationSprite(path);
				sprite.init();
				sprite.play(path);
				sprite.node.Name = "floor";

				sprite.node.Position = new Vector2(floor_head, 200);
				bridge.AddChild(sprite.node);

				floor_head += width;

			}

			// Let's also put down some arches!
			for (int i = 0; i < 30; i++) {
				const float width = 325 - 2;
				var sprite = new objects.images.AnimationSprite("vanilla:collections/City_Arch/new_city_entrance_trimmed_0004_1");
				sprite.init();
				sprite.play("vanilla:collections/City_Arch/new_city_entrance_trimmed_0004_1");
				sprite.node.Name = "arch";
				sprite.node.Position = new Vector2(width * i, 520);
				bridge.AddChild(sprite.node);
			}

			bridge.Position = new Vector2(0, 200);

			// Let's place some water.
			var water_node = this.GetNode<Node2D>("Water");
			// var water_shader = (Shader)ResourceLoader.Load("res://resources/shaders/WaterShader.shader");

			var water_color = new Color(0.0f, 0.2f, 0.8f, 0.7f);

			for (int i = -30; i < 30; i++) {
				// this isn't really animated; I don't really care right now.
				var sprite = new objects.images.AnimationSprite("vanilla:sprites/water_top_solid_000");
				sprite.init();
				sprite.play("vanilla:sprites/water_top_solid_000");
				sprite.node.Position = new Vector2(800 * i, 0);

				water_node.AddChild(sprite.node);

				// Sadly this doesn't seem to work.
				// sprite.set_shader((Shader)water_shader.Duplicate(true));
				// But this does. Not as good, though.
				sprite.node.Modulate = water_color;
			}
			// For fun let's also just put a big rectangle to simulate water beneath it.
			var water_rect = new ColorRect();
			water_rect.Name = "water_rect";
			water_rect.Color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			water_rect.Modulate = water_color;

			// Center it, and place it underneath the waves as well.
			water_rect.RectPosition = new Vector2(-30 * 800, 30);

			water_rect.RectSize = new Vector2(60 * 800, 900);

			water_node.AddChild(water_rect);

		}
	}
}
