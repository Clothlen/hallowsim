namespace Hallowsim.scenes.world.player {
	using Godot;

	public class Player
	{

		public readonly RigidBody2D node;

		// public objects.entities.AbstractEntity entity { get; private set; }

		private Hallowsim.player.IPlayerControllable _controller;

		public Player() {
			// this.entity = new objects.entities.mobs.knight.Knight();
			// this.entity.init();
			// this.entity.create();

			var scene = ResourceLoader.Load<PackedScene>("res://source/scenes/world/mobs/knight/Knight.tscn");
			var instance = scene.Instance();

			this.node = (mobs.AbstractMob)instance;
			this._controller = (Hallowsim.player.IPlayerControllable)instance;
		}

		public void input(Godot.InputEvent evt) {

			if (evt.IsActionPressed("player_attack")) {
				this._controller.player_melee_attack();
			}

		}

	}
}
