namespace Hallowsim.scenes {
	using Godot;

	internal class CoreNode : Node
	{

		public override void _UnhandledKeyInput(InputEventKey evt) {
			if (evt.IsActionPressed("quit")) {
				this.GetTree().Quit();
			}
		}
	}
}
