namespace Hallowsim.objects {
	public abstract class WorldObject
	{

		public readonly util.objects.AspectContainer aspects = new util.objects.AspectContainer();

		public readonly util.objects.ServiceContainer services = new util.objects.ServiceContainer();

		public virtual void init() { }

		public virtual void create() {
			this.aspects.start();
			this.services.start();
		}

		public virtual void destroy() {
			this.aspects.stop();
			this.services.stop();
		}

	}
}
