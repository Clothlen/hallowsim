namespace Hallowsim.objects.aspects {
	public class WorldAspect : util.objects.AbstractAspect
	{

		public WorldObject world_object;

		public void initialize_aspect(util.objects.AspectContainer container, WorldObject wo) {
			this.world_object = wo;
			base.initialize_aspect(container);
		}

	}
}
