namespace Hallowsim.objects {
	public interface INodeExposer
	{
		Godot.Node2D node { get; }
	}
}
