namespace Hallowsim.objects.images {

	using System;
	using System.IO;
	using Godot;

	public class AnimationSprite : INodeExposer
	{

		public const float FPS_MULTIPLIER = 0.75f;

		public Node2D node { get { return this._node; } }

		public readonly string[] resources;

		private AnimatedSprite _node;

		public AnimationSprite(params string[] res) {
			this.resources = res;
		}

		public void init() {
			var spriteframes = new SpriteFrames();

			this.add(spriteframes, this.resources);

			this._node = new AnimatedSprite();
			this._node.Frames = spriteframes;
		}

		public void add(params string[] resources) {
			this.add(this._node.Frames, resources);
		}

		public void add(SpriteFrames spriteframes, params string[] resources) {
			foreach (var resource in resources) {
				// Check to see what they want.
				var resource_split = resource.Split(new[] { ':' }, 2);
				if (resource_split.Length != 2) {
					throw new ArgumentException("Given resource does not split into two parts.");
				}

				var domain = resource_split[0];
				var path = resource_split[1];

				if (path.StartsWith("clips")) {
					// It's a clip.
					// What is it referring to?
					var name = path.Substring("clips/".Length, path.Length - "clips/".Length);
					var clip = assets.AssetLoader.get_clip(domain, name);

					spriteframes.AddAnimation(resource);
					spriteframes.SetAnimationSpeed(resource, clip.fps * FPS_MULTIPLIER);

					if (
						clip.wrap_mode == assets.classes.Clip.ClipWrapMode.LOOP
						||
						clip.wrap_mode == assets.classes.Clip.ClipWrapMode.LOOP_PART
					) {
						spriteframes.SetAnimationLoop(resource, true);
					}

					foreach (var frame in clip.frames) {
						var tex = new ImageTexture();
						tex.CreateFromImage(frame);
						spriteframes.AddFrame(resource, tex);
					}

				}
				else if (path.StartsWith("collections") || path.StartsWith("sprites")) {
					// They just want a single image.

					var image = assets.AssetLoader.get_image(domain, path);
					var tex = new ImageTexture();
					tex.CreateFromImage(image);

					spriteframes.AddAnimation(resource);
					spriteframes.AddFrame(resource, tex);

				}
				else {
					throw new FileNotFoundException($"Could not find resource for {resource}");
				}

			}
		}

		public void play(string name) {
			this._node.Play(name);
			#if DEBUG
				if (this._node.Animation != name) {
					throw new ArgumentException($"No animation with the name `{name}` exists.");
				}
			#endif
		}

		public bool flip_horizontal {
			get {
				return this._node.FlipH;
			}
			set {
				this._node.FlipH = value;
			}
		}

		public bool flip_vertical {
			get {
				return this._node.FlipV;
			}
			set {
				this._node.FlipV = value;
			}
		}

	}
}
