namespace Hallowsim.assets.classes {

	using System;
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptIn, ItemRequired=Required.Always)]
	public class Clip
	{
		[JsonProperty("name")]
		public string name;

		[JsonProperty("frames")]
		public string[] frame_names;

		[JsonProperty("fps")]
		public float fps;

		[JsonProperty("loop_start")]
		public int loop_start;

		[JsonProperty("wrap_mode")]
		public int raw_wrap_mode;

		public ClipWrapMode wrap_mode;

		public Godot.Image[] frames;

		public static Clip get(string path) {
			var text = System.IO.File.ReadAllText(path);

			var clip = JsonConvert.DeserializeObject<Clip>(text);

			// Probably bad way of doing this. Too bad!
			clip.wrap_mode = (ClipWrapMode)Enum.Parse(typeof(ClipWrapMode), clip.raw_wrap_mode.ToString());

			clip.frames = new Godot.Image[clip.frame_names.Length];
			for (int i = 0; i < clip.frame_names.Length; i++) {
				clip.frames[i] = AssetLoader.get_image(clip.frame_names[i]);
			}

			return clip;
		}

		public enum ClipWrapMode
		{
			LOOP = 0,
			LOOP_PART = 1,
			ONCE = 2,
			PING_PONG = 3,
			RANDOM_FRAME = 4,
			// Not sure about 5
			SINGLE = 6
		}

	}
}
