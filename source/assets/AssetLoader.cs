namespace Hallowsim.assets {
	using System;
	using System.Collections.Generic;
	using System.IO;
	using Godot;

	public static class AssetLoader
	{

		private const string _mods_godot_dir = "user://mods";

		private static readonly string _mods_dir = ProjectSettings.GlobalizePath(_mods_godot_dir);

		private static readonly Dictionary<string, Dictionary<string, Dictionary<string, classes.Clip>>> _clip_data_collection = new Dictionary<string, Dictionary<string, Dictionary<string, classes.Clip>>>();

		// key1 == modid, key2 == file path
		private static readonly Dictionary<string, Dictionary<string, Image>> _image_collection = new Dictionary<string, Dictionary<string, Image>>();

		private static readonly Dictionary<string, Dictionary<string, Image>> _sprite_collection = new Dictionary<string, Dictionary<string, Image>>();

		public static classes.Clip get_clip(string identifier) {
			var (modid, location) = _split_identifier(identifier);
			return get_clip(modid, location);
		}

		public static classes.Clip get_clip(string modid, string location) {
			var path = location.Split(new char[] { '/' });
			if (path.Length != 2) {
				throw new ArgumentException("Could not split path into 2 parts.");
			}

			return get_clip(modid, path[0], path[1]);
		}

		public static classes.Clip get_clip(string modid, string category, string name) {
			// TODO: try-catch might be better.
			if (_clip_data_collection.ContainsKey(modid)) {
				if (_clip_data_collection[modid].ContainsKey(category)) {
					if (_clip_data_collection[modid][category].ContainsKey(name)) {
						return _clip_data_collection[modid][category][name];
					}
				}
				else {
					_clip_data_collection[modid].Add(category, new Dictionary<string, classes.Clip>());
				}
			}
			else {
				_clip_data_collection.Add(
					modid,
					new Dictionary<string, Dictionary<string, classes.Clip>>() {
						{category, new Dictionary<string, classes.Clip>()}
					}
				);
			}

			var clip = _get_clip(modid, category, name);

			_clip_data_collection[modid][category].Add(name, clip);
			return clip;

		}

		public static Image get_image(string identifier) {
			var (modid, location) = _split_identifier(identifier);
			return get_image(modid, location);
		}

		public static Image get_image(string modid, string path) {
			if (_image_collection.ContainsKey(modid)) {
				if (_image_collection[modid].ContainsKey(path)) {
					return _image_collection[modid][path];
				}
			}
			else {
				_image_collection.Add(modid, new Dictionary<string, Image>());
			}

			var image = new Image();
			image.Load($"{_mods_dir}/{modid}/images/{path}.png");

			_image_collection[modid].Add(path, image);
			return image;

		}

		public static void clear() {
			_clip_data_collection.Clear();
			_image_collection.Clear();
			_sprite_collection.Clear();
		}

		private static classes.Clip _get_clip(string modid, string category, string name) {
			var mod_dir = _get_mods_dir(modid);

			var clips_dir = System.IO.Path.Combine(mod_dir, "images", "clips");

			var category_dir = System.IO.Path.Combine(clips_dir, category);

			var requested_file = System.IO.Path.Combine(category_dir, $"{name}.json");

			if (!System.IO.File.Exists(requested_file)) {
				throw new FileNotFoundException($"The requested clip {modid}:{category}/{name} does not exist at {requested_file}.");
			}

			return classes.Clip.get(requested_file);

		}

		private static string _get_mods_dir(string modid) {
			var dir = System.IO.Path.Combine(_mods_dir, modid);
			if (!System.IO.Directory.Exists(dir)) {
				throw new DirectoryNotFoundException($"Given mod {modid} does not exist.");
			}

			return dir;
		}

		private static (string, string) _split_identifier(string identifier) {
			var splitted = identifier.Split(new char[] { ':' }, 2);
			if (splitted.Length != 2) {
				throw new ArgumentException("Could not split identifier.");
			}

			return (splitted[0], splitted[1]);
		}

	}
}
